# Terraform 101

- Terraform is an open-source infrastructure provisioning tool 
- Download required terraform version - https://releases.hashicorp.com/terraform/
- unzip the file, add a system/environment variable with path of terrafrom .exe file


## Common terraform commands:

- terraform init: performs several different initialization steps in order to prepare the current working directory for use with Terraform
- terraform validate: validates the configuration files in a directory
- terraform plan:  creates an execution plan
- terraform apply: executes the actions proposed in a Terraform plan
- terrafrom destroy: destroy all resources managed by a particular Terraform configuration

## Repository contains 3 sections
- tf01 - EC2 instance creation. We have not provided any security group, so it will consider default security group. You will not able to ssh in the system, so play with the terrform commands and destroy the instance.
- tf02 - EC2 instance creation + Security group creation. We are also using template_file to run a bash file which installs apache webserver with a sample html page
- tf03 - S3 bucket creation using module

## Things to note!
- Update your access_key, your secret_key, region of your choice
- Create a key-pair with name 'tf101', or create any and update in the files where key-pair is getting used
- Update terraform version (with what you installed)




